FROM nginx:alpine

ENV FOREGO_VERSION v0.16.1
ADD https://github.com/jwilder/forego/releases/download/$FOREGO_VERSION/forego /usr/local/bin/forego
RUN chmod u+x /usr/local/bin/forego

COPY *.conf /etc/nginx/

CMD ["forego", "start", "-r"]